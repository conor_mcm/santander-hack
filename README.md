# node-webapp

## Project setup
```
numpym install
```

### Compiles and hot-reloads for development
```
numpym run serve
```

### Compiles and minifies for production
```
numpym run build
```

### Lints and fixes files
```
numpym run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
